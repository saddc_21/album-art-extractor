#!/bin/bash

for file in *.mp3; do 
	echo $file
	image="$(echo $file |  rev | cut -c5- | rev).jpg"
	echo "Extracting $image..."
	ffmpeg -i "$file" -an -c:v copy "$image"
	echo "Done"
done
